@extends('layout.master')
@section('judul')
	Halaman Edit Cast berid{{$cast->id}}
@endsection
@section('isi')
<div>
    <h2>Edit Cast {{$cast->id}}</h2>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="title" placeholder="Masukkan Nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Umur</label>
            <input type="text" class="form-control" name="umur"  value="{{$cast->umur}}"  id="body" placeholder="Masukkan Body">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Biodata</label>
            <input type="text" class="form-control" name="biodata"  value="{{$cast->biodata}}"  id="body" placeholder="Masukkan Body">
            @error('biodata')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection